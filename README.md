# Changelog ICTShop

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

# [3.1.0.18] - 2019-08-28
## Penjualan Barang
- **Changed :**
	1. Ubah nama user di bawah cetak faktur A5 dan A5 Continuous sesuai user yang login.

# [3.1.41.17] - 2019-07-04
## Sistem
- **Fixed :**
	1. Beberapa tampilan menu ada yang terpotong di layar netbook 10 inchi.

# [3.1.41.16] - 2019-04-15
## Menu Retur Pembelian Barang
- **Changed :**
	1. Optimasi sistem.
- **Fixed :**
	1. Fix salah hitung total retur.

# [3.1.41.15] - 2018-09-26
## Menu Retur Pembelian Barang
- **Fixed :**
	1. Fix salah hitung jumlah maksimum barang yang bisa diretur.

## Menu Penjualan Barang
- **Changed :**
	1. Optimasi transaksi penjualan barang supaya lebih cepat.

## Menu laporan Penjualan Barang
- **Changed :**
	1. Tambah informasi diskon faktur dan ongkos kirim di footer tabel data.
	2. Tambah informasi diskon faktur dan ongkir pada laporan penjualan berdasarkan no. faktur.
	3. Tambah informasi diskon faktur pada laporan penjualan berdasarkan pelanggan.
	4. Tambah informasi diskon faktur pada laporan penjualan berdasarkan item.
	5. Tambah informasi diskon faktur pada laporan penjualan berdasarkan sales.
	6. Tambah informasi diskon faktur pada laporan penjualan berdasarkan supplier.
	7. Ubah ukuran font di semua hasil cetak laporan penjualan.

# [3.1.41.14] - 2018-05-22
## Menu Laporan Laba(Rugi)
- **Fixed :**
	1. Fix informasi periode tidak muncul pada judul laporan ketika dicetak.

# [3.1.41.13] - 2018-03-12
## Menu Ganti Shift
- **Fixed :**
	1. Fix perhitungan nominal retur penjualan tipe hutang.
## Menu Penjualan Barang
- **Added :**
	1. Tambah kolom nominal uang muka penjualan tipe hutang pada data transaksi.
## Menu Import Data Barang Lengkap
- **Fixed :**
	1. Fix data barang yang diimport belum masuk ke data pembelian barang. 

# [3.1.41.12] - 2017-07-11
## Menu Pembelian Barang
- **Fixed :**
	1. Fix tidak bisa melakukan pembelian dengan barang yang sama.